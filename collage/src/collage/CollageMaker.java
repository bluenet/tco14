package collage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Random;
import java.util.Set;





public class CollageMaker {

	final int initdiv = 5;
	final int loop = 20;
	final int range = 30;
	final boolean usebest = false;

	static int th;
	static int tw;
	static Image target;

	final int I = 200;

	public int[] compose(int[] data){

		int N = data.length;
		int[] res = new int[I*4];


		th = data[0];
		tw = data[1];
		System.out.println(th + " " + tw);

		target = new Image(th,tw,-1,new int[th][tw],0);
		for(int i = 0; i < th*tw; i++){
			target.pixels[i/tw][i%tw] = data[i+2];
		}



		int index = th*tw+2;
		for(int id = 0; id < I; id++) {
			int ih = data[index++];
			int iw = data[index++];
			int[][] pixcels = new int[ih][iw];
			//System.out.println(ih +"  "+iw);
			for(int j = 0; j < ih*iw; j++){
				pixcels[j/iw][j%iw] = data[index+j];
			}
			Image image = new Image(ih,iw,id,pixcels);

			Collage.rawImages[id] = image;
			Collage.orderedRawImages[id] = image;
			index += ih*iw;


		}

		Image[] rawImages = Collage.rawImages;
		Image[] orderedRawImages = Collage.orderedRawImages;

		Arrays.sort(orderedRawImages,new Comparator<Image>() {
			@Override
			public int compare(Image o1, Image o2) {
				return (int)(o1.ave-o2.ave);
			}
		});


		Arrays.fill(res, -1);

		System.out.println("tukuru");

		int minsum = Integer.MAX_VALUE;
		int[] div = {initdiv,2};

		for(int i = 3; i <= 8; i++){
			for(int j = 3; j <= 8; j++){
				for(int l = 0; l < 10; l++){

					Collage col = new Collage();
					Comp tcmp = new Comp(-1,0,0, th-1,tw-1,Integer.MAX_VALUE);
					col.components.add(tcmp);
					
					col.components = col.refill(i,j,l,n);
					List<Comp> tcomponents = c.components;
					int sum = 0;
					if(tcomponents  == null){
						continue;
					}

					cm.refill();

					for(Comp cmp:tcomponents){
						sum+=cmp.d;
					}
					if(minsum > sum){
						minsum = sum;
						components = tcomponents;
					}
				}
			}
		}



		for(Comp cmp:components){
			reg(cmp);
		}



		for(int l = 0; l < loop; l++) {
			Collections.sort(components,descending );

			for(int i = 0; i<30; i++){
				System.out.println("size"+components.size());
				Comp cmp = null;
				while(true){
					cmp = components.remove(0);
					if(idtoComp[cmp.id] == null)continue;
					else if(!idtoComp[cmp.id].equals(cmp))continue;
					else break;
				}
				boolean b = false;

				for(int j = 2; j <= 4; j++){
					for(int k = 1; k <= 4; k++){
						if(rnd.nextInt(2) != 0){
							int t = k;
							k = j;
							j = t;
						}
						release(cmp.id);
						List<Comp> t = fill(cmp,j,k,cmp.d);
						if(t == null){
							reg(cmp);

						} else{
							for(Comp c:t){
								reg(c);
							}
							components.addAll(t);
							b = true;
							break;
						}
					}
					if(b)break;
				}
				if(!b)components.add(cmp);
			}

		}



		//System.out.println(fh + " " + fw);


		while(!components.isEmpty()){
			Comp cmp = components.remove(0);
			int id = cmp.id;
			res[id*4] = cmp.top;
			res[id*4+1] = cmp.left;
			res[id*4+2] = cmp.bottom;
			res[id*4+3] = cmp.right;
		}
		/*

		for(int id = 0; id < I; id++){
			if(!used[id])continue;
			Comp comp = idtoComp[id];
			System.out.println(comp.d/(comp.H*comp.W));
			res[id*4] = comp.top;
			res[id*4+1] = comp.left;
			res[id*4+2] = comp.bottom;
			res[id*4+3] = comp.right;
			System.out.println(res[id*4] +" " +res[id*4+1] + " " + res[id*4+2] + " " + res[id*4+3]);
		}*/
		return res;

	}



}

class Collage {


	static Image[] rawImages = new Image[200];
	static List<Comp> components = new ArrayList<>();

	boolean[] used = new boolean[200];

	Random rnd =  new Random(2);

	Comparator<Comp> descending = new Comparator<Comp>() {

		public int compare(Comp o1, Comp o2) {
			double c1 = (double)o1.d/(o1.H*o1.W);
			double c2 = (double)o2.d/(o2.H*o2.W);

			return (int) ((int)-c1 + c2);
		}
	};


	Comparator<Comp>  ascending = new Comparator<Comp>() {

		public int compare(Comp o1, Comp o2) {
			double c1 = (double)o1.d/(o1.H*o1.W);
			double c2 = (double)o2.d/(o2.H*o2.W);

			return (int) ((int)c1 - c2);
		}
	};


	Comp[] idtoComp = new Comp[200];


	static Image[] orderedRawImages = new Image[200];





	public List<Comp> fill(Comp cmp,int d0, int d1,int d){

		int top = cmp.top;
		int bottom = cmp.bottom;
		int left = cmp.left;
		int right = cmp.right;

		//System.out.println("region:"+top+" "+left+" "+bottom+" "+right);
		//System.out.println("fill");

		int H = bottom-top+1;
		int W = right-left+1;
		if(H < d0 || W < d1)return null;

		int sum = 0;
		boolean tused[] = new boolean[I];
		List<Comp> res = new ArrayList<Comp>(d0*d1);


		int[] bottoms = new int[d0];
		int[] rights = new int[d1];

		{
			int bb = top-1;
			for (int i = 0; i < d0-1; i++) {
				int to = bb+1;
				int bo = (H/d0)-1+to;
				int p =(int)H/(d0*2)==0?0: rnd.nextInt((int) (H/(d0*2)))*rnd.nextInt(2)==1?-1:1;
				bottoms[i] = bo+(i != d0-1?p:0);
				bb = bo;

			}
			bottoms[d0-1] = bottom; 

			int br = left-1;
			for (int j = 0; j < d1; j++) {

				int le= br+1;
				int p = (int)W/(d1*2)==0?0:rnd.nextInt((int)(W/(d1*2)))*rnd.nextInt(2)==1?-1:1;
				int ri = j == d1-1?right:(W/d1)-1+le;
				rights[j] = ri+(j != d1-1?p:0);
				br = ri;
			}
		}


		int bb = top-1;
		for (int i = 0; i < d0; i++) {
			int br = left-1;
			int to = bb+1;
			int bo = bottoms[i];
			for (int j = 0; j < d1; j++) {

				int le= br+1;
				int ri = rights[j];
				br = ri;

				Comp comp = usebest ? best(to,le,bo,ri,tused): better(to,le,bo,ri,tused);
				if(comp.id == -1){
					return null;
				}
				tused[comp.id] = true;
				res.add(comp);
				sum+=comp.d;
			}

			bb=bo;
		}

		if(res.size() != d0*d1)return null;

		if(d < sum)return null;



		return res;
	}


	private void reg(Comp comp){
		used[comp.id] = true;
		idtoComp[comp.id] = comp;
	}


	public List<Comp> refill() {
		Collections.sort(components,descending );
		
		for(int i = 0; i<30; i++){
			System.out.println("size"+components.size());
			Comp cmp = null;
			cmp = components.remove(0);


			boolean b = false;

			for(int j = 2; j <= 4; j++){
				for(int k = 1; k <= 4; k++){
					if(rnd.nextInt(2) != 0){
						int t = k;
						k = j;
						j = t;
					}
					release(cmp.id);
					List<Comp> t = fill(cmp,j,k,cmp.d);
					if(t == null){
						reg(cmp);

					} else{
						for(Comp c:t){
							reg(c);
						}
						components.addAll(t);
						b = true;
						break;
					}
				}
				if(b)break;
			}
			if(!b)components.add(cmp);
		}



		return components;


	}


	/*private void refill(Comp A, Comp B) {
		if(idtoComp[A.id] != A || idtoComp[B.id] != B){//ばぐるかも
			throw new Error("hoge");
		}

		PriorityQueue<Comp> queA = new PriorityQueue<Comp>(1,new Comparator<Comp>(){
			@Override
			public int compare(Comp o1, Comp o2) {
				return o1.d-o2.d;
			}
		});

		PriorityQueue<Comp> queB = new PriorityQueue<Comp>(1,new Comparator<Comp>(){
			@Override
			public int compare(Comp o1, Comp o2) {
				return o1.d-o2.d;
			}
		});

		used.add(A.id);
		used.add(B.id);


		for(int id = 0; id < I; id++){
			Image img = rawImages[id];
			if(!used.contains(id) && img.H >= A.H && img.W >= A.W){
				queA.add(new Comp(id,A.top,A.left,A.bottom,A.right,diff(A.top, A.left, rawImages[id].scale(A.H, A.W))));
			}
			if(!used.contains(id) && img.H >= B.H && img.W >= B.W){
				queB.add(new Comp(id,B.top,B.left,B.bottom,B.right,diff(B.top, B.left, rawImages[id].scale(B.H, B.W))));
			}
		}

		Comp bestA = queA.poll();
		Comp bestB = queB.poll();
		if(bestA.id == bestB.id) {
			if(bestA.d + queB.peek().d > queA.peek().d + bestB.d ) {
				bestB = queB.peek();
			} else {
				bestA = queA.peek();
			}

		}



		reg(bestA);
		reg(bestB);

	}*/

	private void release(int id){
		idtoComp[id] = null;
		used[id] = false;

	}

	static int[][] mem;

	private static void makeSum(){

		int[][] sum = new int[th][tw];
		for(int i = 0; i < th; i++) {
			for(int j = 0; j < tw; j++) {
				for(int k = 0; k < j; k++) {
					sum[i][j]+=target.pixels[i][k];
				}
			}
		}
		for(int w = 0; w < tw; w++){
			int a = 0;
			for(int h = 0; h < th; h++){
				a+=sum[h][w];
				mem[h][w]=a;
			}
		}
	}



	private static double getAverage(int top, int left, int bottom, int right) {

		int H =  bottom-top+1;
		int W = right-left+1;
		int res = 0;
		if(top ==  0&& left == 0) {
			res= mem[bottom][right];
		}
		else if(top == 0){
			res= mem[bottom][right]-mem[bottom][left-1];
		}
		else if(left == 0) {
			res= mem[bottom][right] - mem[top-1][right];
		}
		else {
			res= mem[bottom][right] - mem[top-1][right]-mem[bottom][left-1]+mem[top-1][left-1];
		}
		return (double)res/(H*W);
	}


	private Comp better(int top, int left, int bottom, int right, boolean[] tused) {



		int H = bottom-top+1;
		int W = right-left+1;
		double ave = getAverage(top, left, bottom, right);

		int l = 0;
		int r = I-1;

		while(l < r){
			//System.out.println(l);
			int mid = (l+r)/2;
			if(orderedRawImages[mid].ave < ave) {
				l = mid+1;
			}
			else r = mid-1;

		}

		int s = Math.max(0, l-range/2);
		int min = Integer.MAX_VALUE;
		int minid = -1;
		for(int i = s; i <s+range && i < I; i++){
			Image img = orderedRawImages[i];
			int id = img.id;
			if(used[id] || tused[id]){
				continue;
			}
			int d ;
			if(img.H >= H && img.W >= W && (d = diff(top,left,img.scale(H, W)) )< min){
				min = d;
				minid = img.id;
			}
		}

		return new Comp(minid,top, left, bottom, right, min);

	}

	private Comp best(int top, int left, int bottom, int right,boolean[] tused) {
		int H = bottom-top+1;
		int W = right-left+1;
		int min = Integer.MAX_VALUE;
		int minid = -1;
		for(int id = 0; id < I; id++){
			int d ;
			if(used[id] || tused[id]){
				continue;
			}
			if(rawImages[id].H >= H && rawImages[id].W >= W && (d = diff(top,left,rawImages[id].scale(H, W)) )< min){

				min = d;
				minid = id;

			}
		}

		return new Comp(minid, top, left, bottom, right, min);
	}

	boolean k = false;


	private void change(Comp cmp){



	}

	private boolean isIntersected(int top, int left, Image img, List<Comp> list) {
		int h = img.H;
		int w = img.W;
		for(Comp a:list){
			for(int i = 0; i < 2; i++){
				for(int j = 0; j < 2; j++){
					int r = top+(h-1)*i;
					int c = left+(w-1)*j;
					if(a.top <= r && r <= a.bottom && a.left<=c && c <= a.right)return true;
				}
			}
		}
		return false;
	}

	private int diff(int top, int left, Image image){
		int score = 0;
		for (int r = top; r < top+image.H; r++) {
			for (int c = left; c < left+image.W; c++) {
				int diff = target.pixels[r][c] - image.pixels[r-top][c-left];
				score += diff * diff;
			}
		}
		return score;
	}


}



class Image {

	static Map<Integer,Map<Integer,Image>> scalemap = new HashMap<>();


	int H;
	int W;
	int id;
	int[][] pixels;
	double ave;
	Image(int H, int W, int id, int[][] pixel){
		this.H = H;
		this.W = W;
		this.id = id;
		this.pixels = pixel;

		int sum = 0;
		for(int i = 0; i < H; i++) {
			for (int j = 0; j < W; j++) {
				sum+=pixel[i][j];
			}
		}
		ave = (double)sum/(H*W);

	}

	Image(int H, int W, int id, int[][] pixel,double ave){
		this.H = H;
		this.W = W;
		this.id = id;
		this.pixels = pixel;
		this.ave = ave;
	}

	int intersect(int a, int b, int c, int d) {
		int from = Math.max(a, c);
		int to = Math.min(b, d);
		return from < to ? to - from : 0;
	}

	public Image scale(int newH, int newW) {
		int key = newH*1000+newW;
		if(scalemap.containsKey(key)){
			Map<Integer,Image> map = scalemap.get(key);
			if(map.containsKey(this.id)) {
				return map.get(id);
			}
		} else {
			scalemap.put(key,new HashMap<Integer,Image>());
		}

		List<Integer> origRList = new ArrayList<Integer>();
		List<Integer> newRList = new ArrayList<Integer>();
		List<Integer> intrRList = new ArrayList<Integer>();
		List<Integer> origCList = new ArrayList<Integer>();
		List<Integer> newCList = new ArrayList<Integer>();
		List<Integer> intrCList = new ArrayList<Integer>();

		for (int origR = 0; origR < H; origR++) {
			int r1 = origR * newH, r2 = r1 + newH;
			for (int newR = 0; newR < newH; newR++) {
				int r3 = newR * H, r4 = r3 + H;
				int intr = intersect(r1, r2, r3, r4);
				if (intr > 0) {
					origRList.add(origR);
					newRList.add(newR);
					intrRList.add(intr);
				}
			}
		}

		for (int origC = 0; origC < W; origC++) {
			int c1 = origC * newW, c2 = c1 + newW;
			for (int newC = 0; newC < newW; newC++) {
				int c3 = newC * W, c4 = c3 + W;
				int intr = intersect(c1, c2, c3, c4);
				if (intr > 0) {
					origCList.add(origC);
					newCList.add(newC);
					intrCList.add(intr);
				}
			}
		}

		Image res = new Image(newH, newW,this.id,new int[newH][newW],ave);

		for (int i = 0; i < origRList.size(); i++) {
			int origR = origRList.get(i);
			int newR = newRList.get(i);
			int intrR = intrRList.get(i);

			for (int j = 0; j < origCList.size(); j++) {
				int origC = origCList.get(j);
				int newC = newCList.get(j);
				int intrC = intrCList.get(j);

				res.pixels[newR][newC] += intrR * intrC * pixels[origR][origC];
			}
		}

		for (int r = 0; r < newH; r++) {
			for (int c = 0; c < newW; c++) {
				res.pixels[r][c] = (2 * res.pixels[r][c] + H * W) / (2 * H * W);
			}
		}

		scalemap.get(key).put(id,res);

		return res;
	}
}

class Comp{
	public Comp(int id,int top, int left, int bottom, int right, int d) {
		super();
		this.id = id;
		this.top = top;
		this.left = left;
		this.bottom = bottom;
		this.right = right;
		this.d = d;
		this.H = bottom-top+1;
		this.W = right-left+1;
	}

	@Override
	public String toString() {
		return id + " " + top + " " + left + " " + bottom+ " " + right;
	}

	int id;
	int top;
	int left;
	int bottom;
	int right;
	int d;
	int H;
	int W;

}



class Region{

	public Region(int top, int left, int bottom, int right,int d) {
		this.top = top;
		this.left = left;
		this.bottom = bottom;
		this.right = right;
		this.H = bottom-top+1;
		this.W = right-left+1;
		this.d = d;
	}

	public Region(int top, int left, int bottom, int right, int H, int W ) {
		this.top = top;
		this.left = left;
		this.bottom = bottom;
		this.right = right;
		this.H = bottom-top+1;
		this.W = right-left+1;
		this.d = d;

	}

	int top;
	int left;
	int bottom;
	int right;
	int H;
	int W;
	int d;

}

class Collage{



}

